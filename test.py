import unittest

from inventory import Inventory as controller, inventory as model


class InventoryModelTestCase(unittest.TestCase):

    def test_read_json(self):
        model.inventory
        self.assertTrue(model.data)

    def test_query_category_where_store(self):
        data = model.select('category').where(store=1)
        self.assertEqual(data, [10, 10, 2])

    def test_query_items_where_item_name(self):
        data = model.select('items').where(item_name="pc")
        self.assertEqual(data, [1, 3])

    def test_query_items_where_category(self):
        data = model.select('price').where(category=10)
        self.assertEqual(data, [100, 20, 10, 50])

class InventoryControllerTestCase(unittest.TestCase):

    def test_category_where_store(self):
        data = controller.get_categories_for_store(1)
        self.assertEqual(data, [10, 10, 2])

    def test_sum_items_where_item_name(self):
        data = controller.get_item_inventory("pc")
        self.assertEqual(data, 4)

    def test_median_items_where_category(self):
        data = controller.get_median_for_category(10)
        self.assertEqual(data, 35.0)

if __name__ == '__main__':
    unittest.main()
