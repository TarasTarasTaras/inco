import json
from flask import Flask, Response
from inventory import Inventory

app = Flask(__name__)


@app.route('/categories/stores/<int:store_id>', methods=['GET'])
def CategoriesHandler(store_id):
	data = Inventory.get_categories_for_store(store_id)
	return Response(response=json.dumps({'data': data}), status=200, content_type='application/json')


@app.route('/sum/items/<item_name>', methods=['GET'])
def ItemsHandler(item_name):
	data = Inventory.get_item_inventory(item_name)
	return Response(response=json.dumps({'data': data}), status=200, content_type='application/json')


@app.route('/median/prices/categories/<int:category_id>', methods=['GET'])
def PricesHandler(category_id):
	data = Inventory.get_median_for_category(category_id)
	return Response(response=json.dumps({'data': data}), status=200, content_type='application/json')

