import json

from operator import itemgetter


class BaseModel:

    allowed_fields = ["store", "category", "item_name", "items", "price"]

    def __init__(self, path, data=None):
        self.path = path
        self.data = data

    def read(self):
        with open(self.path) as fp:
            self.data = json.load(fp)


class InventoryModel(BaseModel):

    @property
    def inventory(self):
        if self.data is None:
            self.read()
        return self.data

    def select(self, *args):
        return Select(self.inventory, *args)


class Select:

    def __init__(self, data, *fields):
        self.data = data
        self.fields = fields

    @staticmethod
    def _check(item, conditions):
        for key, value in conditions.items():
            if item[key] != value:
                return False
        return True

    def _filter(self, conditions):
        selection = []
        for item in self.data:
            if self._check(item, conditions):
                selection.append(item)
        return selection

    def where(self, **conditions):
        fields = itemgetter(*self.fields)
        return [fields(item) for item in self._filter(conditions)]


inventory = InventoryModel('inventory.txt')
