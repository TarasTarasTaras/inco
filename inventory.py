import statistics

from models import inventory


class Inventory(object):
    """
    Inventory Management system 
    """
    @staticmethod
    def get_categories_for_store(store):
        """
        Given a store id you should return all the categories ids in the inventory.
        :param store: store id
        :return: all the categories ids in the inventory
        """
        return inventory.select('category').where(store=store)

    @staticmethod
    def get_item_inventory(item):
        """
        Given items name return all the items across all stores.
        :param item: item name
        :return: all the items across all stores
        """
        query = inventory.select('items').where(item_name=item)
        if query:
            return sum(inventory.select('items').where(item_name=item))
        return []

    @staticmethod
    def get_median_for_category(category):
        """
        Given category id return the median of the prices for all items in the category.
        :param category: category name
        :return: the median of the prices for all items in the category
        """
        query = inventory.select('price').where(category=category)
        return statistics.median(query) if query else []
